import Vue from 'vue'
import VueRouter from 'vue-router'
import Trading from '../views/Trading.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/grafico-avanzado/:especie',
    name: 'Trading',
    component: Trading,
    title:  "Grafico avanzado | Rava Graficos"
  },
  // {
  //   path: '/',
  //   name: 'TradingTest',
  //   component: TradingTest,
  //   title:  "Grafico avanzado | Rava Graficos"
  // },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
